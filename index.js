import renderTemplate from './src/template.js';
import fs from 'fs';
import data from './src/data.js';

const template = fs.readFileSync('./src/template.html', 'utf8');
fs.writeFileSync(`./public/index.html`, renderTemplate(template)(data));