function parse(template, delimiters = '{}') {
  const code = replaceStatements(template, delimiters);
  const precompiledCode = new Function('precompile', `return precompile\`${code}\`;`)(precompile);
  const render = new Function('context', `with (context) {${precompiledCode}}`);
  return render;
}

function precompile(strings, ...values) {
  let str = "let o = '';";
  for (let i = 0; i < values.length; i++) {
    str += `o+=\`${strings[i]}\`;`;
    str += values[i];
  }
  str += `o+=\`${strings[strings.length - 1]}\`;return o;`;
  return str;
}

function replaceStatements(template, delimiters) {
  const [left, right] = delimiters;
  const FOR_VALUE = `\${'for (let $1 of Object.values($2)) {'}`;
  const FOR_KEY_VALUE = `\${'for (let [$2, $1] of Object.entries($3)) {'}`;
  const IF = `\${'if ($1) {'}`;
  const ELSE_IF = `\${'} else if ($1) {'}`;
  const ELSE = `\${'} else {'}`;
  const re = middle => new RegExp(`${left}\s*${middle}\s*${right}`, 'gi');
  return template
    .replace(/\${/g, '\\${')
    .replace(re(`(/for|/if|endfor|endif)`), `\${'}'}`)
    .replace(re(`for (.+), (.+) of (.+)`), FOR_KEY_VALUE)
    .replace(re(`for (.+) of (.+)`), FOR_VALUE)
    .replace(re(`if (.+)`), IF)
    .replace(re(`else if (.+)`), ELSE_IF)
    .replace(re(`else`), ELSE)
}

export default parse;